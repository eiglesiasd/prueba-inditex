package com.prueba.inditex.utils;

import com.prueba.inditex.controller.model.dto.input.PruebaInditexControllerIDTO;
import com.prueba.inditex.controller.model.dto.output.PruebaInditexControllerODTO;
import com.prueba.inditex.repository.model.Brands;
import com.prueba.inditex.repository.model.PruebaInditexRepositoryMO;
import com.prueba.inditex.service.dto.input.PruebaInditexServiceIDTO;
import com.prueba.inditex.service.dto.output.PruebaInditexServiceODTO;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class GenerateProducts {

    public static PruebaInditexControllerIDTO generateControllerIDTO(String date) {

        return PruebaInditexControllerIDTO.builder()
                .brandId(1)
                .date(date)
                .productId(35455)
                .build();
    }

    public static PruebaInditexServiceIDTO generateServiceIDTO(String date) {

        return PruebaInditexServiceIDTO.builder()
                .brandId(1)
                .date(date)
                .productId(35455)
                .build();
    }

    public static PruebaInditexRepositoryMO generateRespositoryMO(String startDate, String endDate, Float price, Integer priceList, Integer priority) {
        PruebaInditexRepositoryMO response = new PruebaInditexRepositoryMO();
        response.setId(1);
        response.setBrands(generateBrands());
        response.setCurr("EUR");
        response.setEndDate(LocalDateTime.parse(endDate, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        response.setPrice(price);
        response.setPriceList(priceList);
        response.setPriority(priority);
        response.setProductId(35455);
        response.setStartDate(LocalDateTime.parse(startDate, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        return response;
    }

    public static Brands generateBrands() {
        Brands brands = new Brands();
        brands.setId(1);
        brands.setName("ZARA");
        return brands;
    }

    public static PruebaInditexServiceODTO generateServiceODTO(String startDate, String endDate, Float price, Integer priceList) {
        PruebaInditexServiceODTO response = new PruebaInditexServiceODTO();
        response.setBrandId(1);
        response.setEndDate(endDate);
        response.setPrice(price);
        response.setPriceList(priceList);
        response.setProductId(35455);
        response.setStartDate(startDate);
        return response;
    }

    public static PruebaInditexControllerODTO generateControllerODTO(String startDate, String endDate, Float price, Integer priceList) {

        return PruebaInditexControllerODTO.builder()
                .brandId(1)
                .endDate(endDate)
                .price(price)
                .priceList(priceList)
                .productId(35455)
                .startDate(startDate)
                .build();
    }
}
