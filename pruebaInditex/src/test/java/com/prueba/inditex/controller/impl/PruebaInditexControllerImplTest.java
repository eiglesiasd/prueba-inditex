package com.prueba.inditex.controller.impl;

import static com.prueba.inditex.utils.GenerateProducts.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;
import com.prueba.inditex.controller.mapper.PruebaInditexControllerMapper;
import com.prueba.inditex.controller.model.dto.input.PruebaInditexControllerIDTO;
import com.prueba.inditex.controller.model.dto.output.PruebaInditexControllerODTO;
import com.prueba.inditex.repository.model.PruebaInditexRepositoryMO;
import com.prueba.inditex.service.PruebaInditexService;
import com.prueba.inditex.service.dto.input.PruebaInditexServiceIDTO;
import com.prueba.inditex.service.dto.output.PruebaInditexServiceODTO;
import com.prueba.inditex.service.mapper.PruebaInditexServiceMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class PruebaInditexControllerImplTest {

    @InjectMocks
    private PruebaInditexControllerImpl controller;

    @Mock
    private PruebaInditexControllerMapper mapper;

    @Mock
    private PruebaInditexServiceMapper serviceMapper;

    @Mock
    private PruebaInditexService service;

    @Test
    public void testFindFinalPriceWithPriceList1() {
        // Given
        final String date = "2020-06-14 10.00.00";
        final String startDate = "2020-06-14 00:00:00";
        final String endDate = "2020-12-31 23:59:59";
        final Integer productId = 35455;
        final Integer brandId = 1;
        final Float price = (float) 35.50;
        final Integer priceList = 1;
        final Integer priority = 0;
        // When
        PruebaInditexControllerIDTO controllerIDTO = generateControllerIDTO(date);
        PruebaInditexServiceIDTO serviceIDTO = generateServiceIDTO(date);
        PruebaInditexRepositoryMO repositoryMO = generateRespositoryMO(startDate, endDate, price, priceList, priority);
        PruebaInditexServiceODTO serviceODTO = generateServiceODTO(startDate, endDate, price, priceList);
        PruebaInditexControllerODTO controllerODTO = generateControllerODTO(startDate, endDate, price, priceList);
        when(mapper.requestToControllerIDTO(date, productId, brandId)).thenReturn(controllerIDTO);
        when(serviceMapper.controllerIdtoToServiceIdto(controllerIDTO)).thenReturn(serviceIDTO);
        when(service.searchPrice(serviceIDTO)).thenReturn(repositoryMO);
        when(serviceMapper.respositoryToServiceODTO(repositoryMO)).thenReturn(serviceODTO);
        when(mapper.serviceOdtoToControllerOdto(serviceODTO)).thenReturn(controllerODTO);

        ResponseEntity<PruebaInditexControllerODTO> responseEntity = controller.findFinalPrice(date, productId, brandId);
        // Then
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(brandId, responseEntity.getBody().getBrandId());
        assertEquals(priceList, responseEntity.getBody().getPriceList());
        assertEquals(price, responseEntity.getBody().getPrice());
        assertEquals(productId, responseEntity.getBody().getProductId());
        assertEquals(startDate, responseEntity.getBody().getStartDate());
        assertEquals(endDate, responseEntity.getBody().getEndDate());
    }

    @Test
    public void testFindFinalPriceWithPriceList2() {
        // Given
        final String date = "2020-06-14 16.00.00";
        final String startDate = "2020-06-14 15:00:00";
        final String endDate = "2020-06-14 18:30:00";
        final Integer productId = 35455;
        final Integer brandId = 1;
        final Float price = (float) 25.45;
        final Integer priceList = 2;
        final Integer priority = 1;
        // When
        PruebaInditexControllerIDTO controllerIDTO = generateControllerIDTO(date);
        PruebaInditexServiceIDTO serviceIDTO = generateServiceIDTO(date);
        PruebaInditexRepositoryMO repositoryMO = generateRespositoryMO(startDate, endDate, price, priceList, priority);
        PruebaInditexServiceODTO serviceODTO = generateServiceODTO(startDate, endDate, price, priceList);
        PruebaInditexControllerODTO controllerODTO = generateControllerODTO(startDate, endDate, price, priceList);
        when(mapper.requestToControllerIDTO(date, productId, brandId)).thenReturn(controllerIDTO);
        when(serviceMapper.controllerIdtoToServiceIdto(controllerIDTO)).thenReturn(serviceIDTO);
        when(service.searchPrice(serviceIDTO)).thenReturn(repositoryMO);
        when(serviceMapper.respositoryToServiceODTO(repositoryMO)).thenReturn(serviceODTO);
        when(mapper.serviceOdtoToControllerOdto(serviceODTO)).thenReturn(controllerODTO);

        ResponseEntity<PruebaInditexControllerODTO> responseEntity = controller.findFinalPrice(date, productId, brandId);
        // Then
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(brandId, responseEntity.getBody().getBrandId());
        assertEquals(priceList, responseEntity.getBody().getPriceList());
        assertEquals(price, responseEntity.getBody().getPrice());
        assertEquals(productId, responseEntity.getBody().getProductId());
        assertEquals(startDate, responseEntity.getBody().getStartDate());
        assertEquals(endDate, responseEntity.getBody().getEndDate());
    }

    @Test
    public void testFindFinalPriceWithPriceList1And2ButDifferentPriority() {
        // Given
        final String date = "2020-06-14 21.00.00";
        final String startDate = "2020-06-14 15:00:00";
        final String endDate = "2020-06-14 18:30:00";
        final Integer productId = 35455;
        final Integer brandId = 1;
        final Float price = (float) 25.45;
        final Integer priceList = 2;
        final Integer priority = 1;
        // When
        PruebaInditexControllerIDTO controllerIDTO = generateControllerIDTO(date);
        PruebaInditexServiceIDTO serviceIDTO = generateServiceIDTO(date);
        PruebaInditexRepositoryMO repositoryMO = generateRespositoryMO(startDate, endDate, price, priceList, priority);
        PruebaInditexServiceODTO serviceODTO = generateServiceODTO(startDate, endDate, price, priceList);
        PruebaInditexControllerODTO controllerODTO = generateControllerODTO(startDate, endDate, price, priceList);
        when(mapper.requestToControllerIDTO(date, productId, brandId)).thenReturn(controllerIDTO);
        when(serviceMapper.controllerIdtoToServiceIdto(controllerIDTO)).thenReturn(serviceIDTO);
        when(service.searchPrice(serviceIDTO)).thenReturn(repositoryMO);
        when(serviceMapper.respositoryToServiceODTO(repositoryMO)).thenReturn(serviceODTO);
        when(mapper.serviceOdtoToControllerOdto(serviceODTO)).thenReturn(controllerODTO);

        ResponseEntity<PruebaInditexControllerODTO> responseEntity = controller.findFinalPrice(date, productId, brandId);
        // Then
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(brandId, responseEntity.getBody().getBrandId());
        assertEquals(priceList, responseEntity.getBody().getPriceList());
        assertEquals(price, responseEntity.getBody().getPrice());
        assertEquals(productId, responseEntity.getBody().getProductId());
        assertEquals(startDate, responseEntity.getBody().getStartDate());
        assertEquals(endDate, responseEntity.getBody().getEndDate());
    }

    @Test
    public void testFindFinalPriceWithPriceList3() {
        // Given
        final String date = "2020-06-15 10.00.00";
        final String startDate = "2020-06-15 00:00:00";
        final String endDate = "2020-06-15 11:00:00";
        final Integer productId = 35455;
        final Integer brandId = 1;
        final Float price = (float) 30.50;
        final Integer priceList = 3;
        final Integer priority = 1;
        // When
        PruebaInditexControllerIDTO controllerIDTO = generateControllerIDTO(date);
        PruebaInditexServiceIDTO serviceIDTO = generateServiceIDTO(date);
        PruebaInditexRepositoryMO repositoryMO = generateRespositoryMO(startDate, endDate, price, priceList, priority);
        PruebaInditexServiceODTO serviceODTO = generateServiceODTO(startDate, endDate, price, priceList);
        PruebaInditexControllerODTO controllerODTO = generateControllerODTO(startDate, endDate, price, priceList);
        when(mapper.requestToControllerIDTO(date, productId, brandId)).thenReturn(controllerIDTO);
        when(serviceMapper.controllerIdtoToServiceIdto(controllerIDTO)).thenReturn(serviceIDTO);
        when(service.searchPrice(serviceIDTO)).thenReturn(repositoryMO);
        when(serviceMapper.respositoryToServiceODTO(repositoryMO)).thenReturn(serviceODTO);
        when(mapper.serviceOdtoToControllerOdto(serviceODTO)).thenReturn(controllerODTO);

        ResponseEntity<PruebaInditexControllerODTO> responseEntity = controller.findFinalPrice(date, productId, brandId);
        // Then
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(brandId, responseEntity.getBody().getBrandId());
        assertEquals(priceList, responseEntity.getBody().getPriceList());
        assertEquals(price, responseEntity.getBody().getPrice());
        assertEquals(productId, responseEntity.getBody().getProductId());
        assertEquals(startDate, responseEntity.getBody().getStartDate());
        assertEquals(endDate, responseEntity.getBody().getEndDate());
    }

    @Test
    public void testFindFinalPriceWithPriceList4() {
        // Given
        final String date = "2020-06-16 21.00.00";
        final String startDate = "2020-06-15 16:00:00";
        final String endDate = "2020-12-31 23:59:59";
        final Integer productId = 35455;
        final Integer brandId = 1;
        final Float price = (float) 38.95;
        final Integer priceList = 4;
        final Integer priority = 1;
        // When
        PruebaInditexControllerIDTO controllerIDTO = generateControllerIDTO(date);
        PruebaInditexServiceIDTO serviceIDTO = generateServiceIDTO(date);
        PruebaInditexRepositoryMO repositoryMO = generateRespositoryMO(startDate, endDate, price, priceList, priority);
        PruebaInditexServiceODTO serviceODTO = generateServiceODTO(startDate, endDate, price, priceList);
        PruebaInditexControllerODTO controllerODTO = generateControllerODTO(startDate, endDate, price, priceList);
        when(mapper.requestToControllerIDTO(date, productId, brandId)).thenReturn(controllerIDTO);
        when(serviceMapper.controllerIdtoToServiceIdto(controllerIDTO)).thenReturn(serviceIDTO);
        when(service.searchPrice(serviceIDTO)).thenReturn(repositoryMO);
        when(serviceMapper.respositoryToServiceODTO(repositoryMO)).thenReturn(serviceODTO);
        when(mapper.serviceOdtoToControllerOdto(serviceODTO)).thenReturn(controllerODTO);

        ResponseEntity<PruebaInditexControllerODTO> responseEntity = controller.findFinalPrice(date, productId, brandId);
        // Then
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(brandId, responseEntity.getBody().getBrandId());
        assertEquals(priceList, responseEntity.getBody().getPriceList());
        assertEquals(price, responseEntity.getBody().getPrice());
        assertEquals(productId, responseEntity.getBody().getProductId());
        assertEquals(startDate, responseEntity.getBody().getStartDate());
        assertEquals(endDate, responseEntity.getBody().getEndDate());
    }

}
