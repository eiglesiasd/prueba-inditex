package com.prueba.inditex.service.impl;

import com.prueba.inditex.repository.PruebaInditexRepository;
import com.prueba.inditex.repository.model.PruebaInditexRepositoryMO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static com.prueba.inditex.utils.GenerateProducts.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class PruebaInditexServiceImplTest {

    @InjectMocks
    private PruebaInditexServiceImpl service;

    @Mock
    private PruebaInditexRepository repository;

    @Test
    public void testFindFinalPriceWithPriceList1() {
        // Given
        final String date = "2020-06-14 10:00:00";
        final String startDate = "2020-06-14 00:00:00";
        final String endDate = "2020-12-31 23:59:59";
        final Integer productId = 35455;
        final Integer brandId = 1;
        final Float price = (float) 35.50;
        final Integer priceList = 1;
        final Integer priority = 0;
        // When
        PruebaInditexRepositoryMO repositoryMO = generateRespositoryMO(startDate, endDate, price, priceList, priority);
        when(repository.findPrices(LocalDateTime.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")), productId, brandId)).thenReturn(repositoryMO);

        PruebaInditexRepositoryMO response = service.searchPrice(generateServiceIDTO(date));
        // Then
        assertEquals(repositoryMO.getPrice(), response.getPrice());
        assertEquals(repositoryMO.getEndDate(), response.getEndDate());
        assertEquals(repositoryMO.getStartDate(), response.getStartDate());
        assertEquals(repositoryMO.getPriceList(), response.getPriceList());
        assertEquals(repositoryMO.getBrands().getId(), response.getBrands().getId());
        assertEquals(repositoryMO.getProductId(), response.getProductId());
    }

    @Test
    public void testFindFinalPriceWithPriceList2() {
        // Given
        final String date = "2020-06-14 16:00:00";
        final String startDate = "2020-06-14 15:00:00";
        final String endDate = "2020-06-14 18:30:00";
        final Integer productId = 35455;
        final Integer brandId = 1;
        final Float price = (float) 25.45;
        final Integer priceList = 2;
        final Integer priority = 1;
        // When
        PruebaInditexRepositoryMO repositoryMO = generateRespositoryMO(startDate, endDate, price, priceList, priority);
        when(repository.findPrices(LocalDateTime.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")), productId, brandId)).thenReturn(repositoryMO);

        PruebaInditexRepositoryMO response = service.searchPrice(generateServiceIDTO(date));
        // Then
        assertEquals(repositoryMO.getPrice(), response.getPrice());
        assertEquals(repositoryMO.getEndDate(), response.getEndDate());
        assertEquals(repositoryMO.getStartDate(), response.getStartDate());
        assertEquals(repositoryMO.getPriceList(), response.getPriceList());
        assertEquals(repositoryMO.getBrands().getId(), response.getBrands().getId());
        assertEquals(repositoryMO.getProductId(), response.getProductId());
    }

    @Test
    public void testFindFinalPriceWithPriceList1And2ButDifferentPriority() {
        // Given
        final String date = "2020-06-14 21:00:00";
        final String startDate = "2020-06-14 15:00:00";
        final String endDate = "2020-06-14 18:30:00";
        final Integer productId = 35455;
        final Integer brandId = 1;
        final Float price = (float) 25.45;
        final Integer priceList = 2;
        final Integer priority = 1;
        // When
        PruebaInditexRepositoryMO repositoryMO = generateRespositoryMO(startDate, endDate, price, priceList, priority);
        when(repository.findPrices(LocalDateTime.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")), productId, brandId)).thenReturn(repositoryMO);

        PruebaInditexRepositoryMO response = service.searchPrice(generateServiceIDTO(date));
        // Then
        assertEquals(repositoryMO.getPrice(), response.getPrice());
        assertEquals(repositoryMO.getEndDate(), response.getEndDate());
        assertEquals(repositoryMO.getStartDate(), response.getStartDate());
        assertEquals(repositoryMO.getPriceList(), response.getPriceList());
        assertEquals(repositoryMO.getBrands().getId(), response.getBrands().getId());
        assertEquals(repositoryMO.getProductId(), response.getProductId());
    }

    @Test
    public void testFindFinalPriceWithPriceList3() {
        // Given
        final String date = "2020-06-15 10:00:00";
        final String startDate = "2020-06-15 00:00:00";
        final String endDate = "2020-06-15 11:00:00";
        final Integer productId = 35455;
        final Integer brandId = 1;
        final Float price = (float) 30.50;
        final Integer priceList = 3;
        final Integer priority = 1;
        // When
        PruebaInditexRepositoryMO repositoryMO = generateRespositoryMO(startDate, endDate, price, priceList, priority);
        when(repository.findPrices(LocalDateTime.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")), productId, brandId)).thenReturn(repositoryMO);

        PruebaInditexRepositoryMO response = service.searchPrice(generateServiceIDTO(date));
        // Then
        assertEquals(repositoryMO.getPrice(), response.getPrice());
        assertEquals(repositoryMO.getEndDate(), response.getEndDate());
        assertEquals(repositoryMO.getStartDate(), response.getStartDate());
        assertEquals(repositoryMO.getPriceList(), response.getPriceList());
        assertEquals(repositoryMO.getBrands().getId(), response.getBrands().getId());
        assertEquals(repositoryMO.getProductId(), response.getProductId());
    }

    @Test
    public void testFindFinalPriceWithPriceList4() {
        // Given
        final String date = "2020-06-16 21:00:00";
        final String startDate = "2020-06-15 16:00:00";
        final String endDate = "2020-12-31 23:59:59";
        final Integer productId = 35455;
        final Integer brandId = 1;
        final Float price = (float) 38.95;
        final Integer priceList = 4;
        final Integer priority = 1;
        // When
        PruebaInditexRepositoryMO repositoryMO = generateRespositoryMO(startDate, endDate, price, priceList, priority);
        when(repository.findPrices(LocalDateTime.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")), productId, brandId)).thenReturn(repositoryMO);

        PruebaInditexRepositoryMO response = service.searchPrice(generateServiceIDTO(date));
        // Then
        assertEquals(repositoryMO.getPrice(), response.getPrice());
        assertEquals(repositoryMO.getEndDate(), response.getEndDate());
        assertEquals(repositoryMO.getStartDate(), response.getStartDate());
        assertEquals(repositoryMO.getPriceList(), response.getPriceList());
        assertEquals(repositoryMO.getBrands().getId(), response.getBrands().getId());
        assertEquals(repositoryMO.getProductId(), response.getProductId());
    }

}
