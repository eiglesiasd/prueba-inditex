package com.prueba.inditex.repository.model;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "prices")
public class PruebaInditexRepositoryMO implements Serializable {

    @Id
    @Column(name = "brand_id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "start_date", nullable = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startDate;

    @Column(name = "end_date", nullable = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endDate;

    @Column(name = "price_list", nullable = false)
    private Integer priceList;

    @Column(name = "product_id", nullable = false)
    private Integer productId;

    @Column(name = "priority", nullable = false)
    private Integer priority;

    @Column(name = "price", nullable = false)
    private Float price;

    @Column(name = "curr", nullable = false)
    private String curr;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Brands.class)
    @JoinColumn(name="brand_id", referencedColumnName = "brand_id", nullable = false, insertable = false, updatable = false)
    private Brands brands;
}
