package com.prueba.inditex.repository;

import com.prueba.inditex.repository.model.PruebaInditexRepositoryMO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;

public interface PruebaInditexRepository extends CrudRepository<PruebaInditexRepositoryMO, Long> {

    @Query(value ="SELECT P.PRODUCT_ID, " +
            "P.BRAND_ID, " +
            "P.PRICE_LIST, " +
            "P.START_DATE, " +
            "P.END_DATE, " +
            "P.PRICE, " +
            "P.CURR, " +
            "P.PRIORITY " +
           "FROM PRICES P " +
           "WHERE P.START_DATE <= :date " +
           "AND P.END_DATE >= :date " +
           "AND P.PRODUCT_ID = :productId " +
           "AND P.BRAND_ID = :brandId", nativeQuery = true)
    PruebaInditexRepositoryMO findPrices(@Param("date") LocalDateTime date, @Param("productId") Integer productId, @Param("brandId") Integer brandId);
}
