package com.prueba.inditex.service.impl;

import com.prueba.inditex.repository.PruebaInditexRepository;
import com.prueba.inditex.repository.model.PruebaInditexRepositoryMO;
import com.prueba.inditex.service.PruebaInditexService;
import com.prueba.inditex.service.dto.input.PruebaInditexServiceIDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class PruebaInditexServiceImpl implements PruebaInditexService {

    PruebaInditexRepository repository;

    @Autowired
    public PruebaInditexServiceImpl(PruebaInditexRepository repository) {
        this.repository = repository;
    }

    @Override
    public PruebaInditexRepositoryMO searchPrice(PruebaInditexServiceIDTO serviceIDTO) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return repository.findPrices(
                LocalDateTime.parse(
                        serviceIDTO.getDate(),
                        formatter),
                serviceIDTO.getProductId(),
                serviceIDTO.getBrandId());
    }
}
