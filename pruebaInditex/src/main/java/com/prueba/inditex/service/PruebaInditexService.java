package com.prueba.inditex.service;

import com.prueba.inditex.repository.model.PruebaInditexRepositoryMO;
import com.prueba.inditex.service.dto.input.PruebaInditexServiceIDTO;

public interface PruebaInditexService {

    PruebaInditexRepositoryMO searchPrice(PruebaInditexServiceIDTO serviceIDTO);
}
