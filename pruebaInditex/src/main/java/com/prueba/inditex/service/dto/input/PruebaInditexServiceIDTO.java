package com.prueba.inditex.service.dto.input;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PruebaInditexServiceIDTO {

    @NotNull
    String date;

    @NotNull
    Integer productId;

    @NotNull
    Integer brandId;
}
