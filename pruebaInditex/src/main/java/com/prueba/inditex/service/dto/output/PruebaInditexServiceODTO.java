package com.prueba.inditex.service.dto.output;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PruebaInditexServiceODTO {

    @NotNull
    Integer brandId;

    @NotNull
    Integer productId;

    @NotNull
    Integer priceList;

    @NotNull
    String startDate;

    @NotNull
    String endDate;

    @NotNull
    Float price;
}
