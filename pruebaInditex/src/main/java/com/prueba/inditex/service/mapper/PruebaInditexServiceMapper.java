package com.prueba.inditex.service.mapper;

import com.prueba.inditex.controller.model.dto.input.PruebaInditexControllerIDTO;
import com.prueba.inditex.repository.model.PruebaInditexRepositoryMO;
import com.prueba.inditex.service.dto.input.PruebaInditexServiceIDTO;
import com.prueba.inditex.service.dto.output.PruebaInditexServiceODTO;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface PruebaInditexServiceMapper {
    PruebaInditexServiceIDTO controllerIdtoToServiceIdto(PruebaInditexControllerIDTO controllerIDTO);

    @Mapping(source = "id", target = "brandId")
    PruebaInditexServiceODTO respositoryToServiceODTO(PruebaInditexRepositoryMO repositoryMO);
}
