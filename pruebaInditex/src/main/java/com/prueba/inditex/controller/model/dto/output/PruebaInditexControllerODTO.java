package com.prueba.inditex.controller.model.dto.output;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PruebaInditexControllerODTO {

    @NotNull
    Integer brandId;

    @NotNull
    Integer productId;

    @NotNull
    Integer priceList;

    @NotNull
    String startDate;

    @NotNull
    String endDate;

    @NotNull
    Float price;

}
