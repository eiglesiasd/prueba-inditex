package com.prueba.inditex.controller.impl;

import com.prueba.inditex.controller.PruebaInditexController;
import com.prueba.inditex.controller.mapper.PruebaInditexControllerMapper;
import com.prueba.inditex.controller.model.dto.input.PruebaInditexControllerIDTO;
import com.prueba.inditex.controller.model.dto.output.PruebaInditexControllerODTO;
import com.prueba.inditex.service.PruebaInditexService;
import com.prueba.inditex.service.dto.input.PruebaInditexServiceIDTO;
import com.prueba.inditex.service.dto.output.PruebaInditexServiceODTO;
import com.prueba.inditex.service.mapper.PruebaInditexServiceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class PruebaInditexControllerImpl implements PruebaInditexController {

    private PruebaInditexControllerMapper mapper;

    private PruebaInditexServiceMapper serviceMapper;

    private PruebaInditexService service;

    @Autowired
    public PruebaInditexControllerImpl (PruebaInditexControllerMapper mapper, PruebaInditexServiceMapper serviceMapper, PruebaInditexService service){
        this.mapper = mapper;
        this.serviceMapper = serviceMapper;
        this.service = service;
    }

    @Override
    public ResponseEntity<PruebaInditexControllerODTO> findFinalPrice(String date, Integer productId, Integer brandId) {

        PruebaInditexControllerIDTO controllerIDTO = mapper.requestToControllerIDTO(
                date,
                productId,
                brandId);

        PruebaInditexServiceIDTO serviceIDTO = serviceMapper.controllerIdtoToServiceIdto(controllerIDTO);
        PruebaInditexServiceODTO serviceODTO = serviceMapper.respositoryToServiceODTO(service.searchPrice(serviceIDTO));

        return new ResponseEntity<>(mapper.serviceOdtoToControllerOdto(serviceODTO), HttpStatus.OK);
    }
}
