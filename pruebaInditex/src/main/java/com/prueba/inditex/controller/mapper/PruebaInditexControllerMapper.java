package com.prueba.inditex.controller.mapper;

import com.prueba.inditex.controller.model.dto.input.PruebaInditexControllerIDTO;
import com.prueba.inditex.controller.model.dto.output.PruebaInditexControllerODTO;
import com.prueba.inditex.service.dto.output.PruebaInditexServiceODTO;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface PruebaInditexControllerMapper {

    PruebaInditexControllerIDTO requestToControllerIDTO(String date, Integer productId, Integer brandId);

    PruebaInditexControllerODTO serviceOdtoToControllerOdto(PruebaInditexServiceODTO serviceODTO);
}
