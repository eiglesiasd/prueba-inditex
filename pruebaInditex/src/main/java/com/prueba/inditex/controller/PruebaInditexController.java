package com.prueba.inditex.controller;

import com.prueba.inditex.controller.model.dto.output.PruebaInditexControllerODTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/search")
public interface PruebaInditexController {

    @GetMapping("/date/{date}/product/{product_id}/brand/{brand_id}")
    ResponseEntity<PruebaInditexControllerODTO> findFinalPrice(@PathVariable("date") String date,
                                  @PathVariable("product_id") Integer productId,
                                  @PathVariable("brand_id") Integer brandId);
}
