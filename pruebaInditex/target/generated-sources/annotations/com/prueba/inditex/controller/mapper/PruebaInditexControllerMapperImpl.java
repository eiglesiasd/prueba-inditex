package com.prueba.inditex.controller.mapper;

import com.prueba.inditex.controller.model.dto.input.PruebaInditexControllerIDTO;
import com.prueba.inditex.controller.model.dto.input.PruebaInditexControllerIDTO.PruebaInditexControllerIDTOBuilder;
import com.prueba.inditex.controller.model.dto.output.PruebaInditexControllerODTO;
import com.prueba.inditex.controller.model.dto.output.PruebaInditexControllerODTO.PruebaInditexControllerODTOBuilder;
import com.prueba.inditex.service.dto.output.PruebaInditexServiceODTO;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-05-04T01:50:02+0200",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 1.8.0_261 (Oracle Corporation)"
)
@Component
public class PruebaInditexControllerMapperImpl implements PruebaInditexControllerMapper {

    @Override
    public PruebaInditexControllerIDTO requestToControllerIDTO(String date, Integer productId, Integer brandId) {
        if ( date == null && productId == null && brandId == null ) {
            return null;
        }

        PruebaInditexControllerIDTOBuilder pruebaInditexControllerIDTO = PruebaInditexControllerIDTO.builder();

        if ( date != null ) {
            pruebaInditexControllerIDTO.date( date );
        }
        if ( productId != null ) {
            pruebaInditexControllerIDTO.productId( productId );
        }
        if ( brandId != null ) {
            pruebaInditexControllerIDTO.brandId( brandId );
        }

        return pruebaInditexControllerIDTO.build();
    }

    @Override
    public PruebaInditexControllerODTO serviceOdtoToControllerOdto(PruebaInditexServiceODTO serviceODTO) {
        if ( serviceODTO == null ) {
            return null;
        }

        PruebaInditexControllerODTOBuilder pruebaInditexControllerODTO = PruebaInditexControllerODTO.builder();

        pruebaInditexControllerODTO.brandId( serviceODTO.getBrandId() );
        pruebaInditexControllerODTO.productId( serviceODTO.getProductId() );
        pruebaInditexControllerODTO.priceList( serviceODTO.getPriceList() );
        pruebaInditexControllerODTO.startDate( serviceODTO.getStartDate() );
        pruebaInditexControllerODTO.endDate( serviceODTO.getEndDate() );
        pruebaInditexControllerODTO.price( serviceODTO.getPrice() );

        return pruebaInditexControllerODTO.build();
    }
}
