package com.prueba.inditex.service.mapper;

import com.prueba.inditex.controller.model.dto.input.PruebaInditexControllerIDTO;
import com.prueba.inditex.repository.model.PruebaInditexRepositoryMO;
import com.prueba.inditex.service.dto.input.PruebaInditexServiceIDTO;
import com.prueba.inditex.service.dto.input.PruebaInditexServiceIDTO.PruebaInditexServiceIDTOBuilder;
import com.prueba.inditex.service.dto.output.PruebaInditexServiceODTO;
import com.prueba.inditex.service.dto.output.PruebaInditexServiceODTO.PruebaInditexServiceODTOBuilder;
import java.time.format.DateTimeFormatter;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-05-04T01:50:02+0200",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 1.8.0_261 (Oracle Corporation)"
)
@Component
public class PruebaInditexServiceMapperImpl implements PruebaInditexServiceMapper {

    @Override
    public PruebaInditexServiceIDTO controllerIdtoToServiceIdto(PruebaInditexControllerIDTO controllerIDTO) {
        if ( controllerIDTO == null ) {
            return null;
        }

        PruebaInditexServiceIDTOBuilder pruebaInditexServiceIDTO = PruebaInditexServiceIDTO.builder();

        pruebaInditexServiceIDTO.date( controllerIDTO.getDate() );
        pruebaInditexServiceIDTO.productId( controllerIDTO.getProductId() );
        pruebaInditexServiceIDTO.brandId( controllerIDTO.getBrandId() );

        return pruebaInditexServiceIDTO.build();
    }

    @Override
    public PruebaInditexServiceODTO respositoryToServiceODTO(PruebaInditexRepositoryMO repositoryMO) {
        if ( repositoryMO == null ) {
            return null;
        }

        PruebaInditexServiceODTOBuilder pruebaInditexServiceODTO = PruebaInditexServiceODTO.builder();

        pruebaInditexServiceODTO.brandId( repositoryMO.getId() );
        pruebaInditexServiceODTO.productId( repositoryMO.getProductId() );
        pruebaInditexServiceODTO.priceList( repositoryMO.getPriceList() );
        if ( repositoryMO.getStartDate() != null ) {
            pruebaInditexServiceODTO.startDate( DateTimeFormatter.ISO_LOCAL_DATE_TIME.format( repositoryMO.getStartDate() ) );
        }
        if ( repositoryMO.getEndDate() != null ) {
            pruebaInditexServiceODTO.endDate( DateTimeFormatter.ISO_LOCAL_DATE_TIME.format( repositoryMO.getEndDate() ) );
        }
        pruebaInditexServiceODTO.price( repositoryMO.getPrice() );

        return pruebaInditexServiceODTO.build();
    }
}
